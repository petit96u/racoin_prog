<?php

use \racoin\common\model\Apikey as Apikey;

function checkApiKey($app){ 
	$app = \Slim\Slim::getInstance(); //récupère l'instance slim
	$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
	$key = $app->request->get('apikey'); //récupère clé d'api ds l'url
	try{	
		//clé d'api = primaryKey
		$keyBdd = Apikey::findOrFail($key); //vérifie que la clé présente dans l'url est dans la table des clés d'api
		$compteur = Apikey::find($key); //trouve la clé dans la base
		$compteur->counter += 1; //incrèmente le compteur correspondant à la clé 
		$compteur->save(); //enregistre la modification
	}	
	catch(\Exception $e){ //création de l'exception si la clé de l'url n'existe pas dans la table des clé d'api
		$app->response->setStatus(403); //statut de l'erreur d'autorisation 403 			
		$tab = ["erreur " => "Autorisation non valide"]; //tableau json contenant le message d'erreur
		echo json_encode($tab); //affichage du tableau
		$app->stop(); //arrêt de l'exécution
	}	
}