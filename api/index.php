<?php 

require '../vendor/autoload.php';
require 'checkApiKey.php';

\racoin\common\Api::EloConfigure('../conf/conf.eloquent.ini');
 
//CHARGEMENT DE SLIM
$app = new \Slim\Slim();
$app->notFound(function () use ($app) { //si le chemin n'est pas trouvé
	$app->response->headers->set('Content-Type', 'application/json'); //renvoie une réponse en json
	$app->response->setStatus(400);	//avec un statut 400
	$tab = ["Erreur " => "La route n'existe pas"]; //et un tableau renvoyant l'erreur (affiché en json)
	echo json_encode($tab); //affichage du tableau 
});
	
//ROUTES
$app->get('/', function(){ //racine
	echo "Bienvenue sur l'API Racoin ! ";
});

$app->get('/categories/:id', 'checkApiKey', function($id){ //1 : obtenir 1 ressource de type catégorie
	$controller = new \racoin\api\controller\CategoriesController();  
	$controller->getCategorie($id);	
	
})->name('categorie');

$app->get('/categories', 'checkApiKey', function(){ //2 : obtenir 1 collection de categories	
	$controller = new \racoin\api\controller\CategoriesController();  
	$controller->getCategories();	
	
})->name('categories');

$app->get('/annonces/:id', 'checkApiKey', function($id){ //3 : obtenir 1 ressource annonce	
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->getAnnonce($id);	
	
})->name('annonce');

$app->get('/annonces', 'checkApiKey', function(){ //4 : obtenir 1 collection d'annonces	
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->getAnnonces();	
	
})->name('annonces');

$app->get('/categorie/:id/annonces', 'checkApiKey', function($id){ //5 : collection d'annonces pour une catégorie
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->getAnnoncesCategorie($id);	
	
})->name('categorieAnnonces');

$app->get('/annonces/:id/categorie', 'checkApiKey', function($id){ //6 : catégorie d'une annonce
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->getCategorieAnnonce($id);	
	
})->name('categorieAnnonce');

$app->post('/annonces', 'checkApiKey', function(){ //11 : ajouter une annonce
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->postAnnonce();	
	
})->name('postAnnonce');

$app->post('/categorie/:id/annonces', 'checkApiKey', function($id){ //12 : ajouter une annonce dans une catégorie
	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->postAnnonceCategorie($id);	
	
})->name('postAnnonceCategorie');

$app->get('/annonces/:id/annonceur', 'checkApiKey', function($id){ //19 : obtenir les coordonnées de l'annonceur
	$controller = new \racoin\api\controller\AnnonceursController();  
	$controller->getCoordAnnonceur($id);	
	
})->name('coordonneesAnnonceur');


$app->put('/annonces/:id/annonceur', 'checkApiKey', function ($id) { //20 : ajouter les coordonnées de l'annonceur
  	$controller = new \racoin\api\controller\AnnonceursController();  
	$controller->putCoordAnnonceur($id);	 
})->name('ajout_coordonneesAnnonceur');

$app->delete('/annonces/:id', 'checkApiKey', function ($id) { //22 : supprimer une annonce
  	$controller = new \racoin\api\controller\AnnoncesController();  
	$controller->deleteAnnonce($id);	 
})->name('delete_annonce');

//EXECUTION / RENDU
$app->run();


