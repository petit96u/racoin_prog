<?php
require '../vendor/autoload.php';

\racoin\common\Api::eloConfigure('../conf/conf.eloquent.ini');

//CHARGEMENT DE SLIM
$app = new \Slim\Slim(['debug'=>true, 'templates.path'=> '../src/racoin/backend/templates']);

$app->view = new \Slim\Views\Twig();
$app->view->setTemplatesDirectory('../src/racoin/backend/templates');

$view = $app->view();
$view->parserOptions = ['debug'=> true];
$view->parserExtensions = [new \Slim\Views\TwigExtension()];

$app->notFound(function () use ($app) { //si le chemin n'est pas trouvé
	$app->response->headers->set('Content-Type', 'text/html'); //renvoie une réponse en html
	$app->response->setStatus(400);	//avec un statut 400
	$tab = "La route n'existe pas"; //et un tableau renvoyant l'erreur 
});

$req = $app->request;
$rootUri = $req->getRootUri();

//ROUTES
$app->get('/', function() use($rootUri){ //formulaire de connexion
	$controller = new \racoin\backend\controller\AdminControllerBE(); 
	$dataController = $controller->login($rootUri);
})->name('login');

$app->post('/', function() use($rootUri){ //formulaire de connexion
	$controller = new \racoin\backend\controller\AdminControllerBE(); 
	$dataController = $controller->login($rootUri);
});

$app->get('/index', function() use ($rootUri){ //liste des annonces à valider 
	$controller = new \racoin\backend\controller\AnnoncesControllerBE();
	$dataController = $controller->annoncesNonValides($rootUri);
})->name('index');

$app->get('/logout', function() use($rootUri){ //déconnexion 
	$controller = new \racoin\backend\controller\AdminControllerBE(); 
	$dataController = $controller->logout($rootUri);
})->name('logout');

$app->get('/annonces/:id', function($id) use ($app,$rootUri){ //105 : afficher le détail d'une annonce
	$controller = new \racoin\backend\controller\AnnoncesControllerBE();  
	$dataController = $controller->annonceDetaillee($id,$rootUri);	
	
})->name('annonceDetaillee');

$app->get('/annoncesValides/:id', function($id) use($rootUri){ //106 : valider une annonce - bouton VALIDER
		$controller = new \racoin\backend\controller\AnnoncesControllerBE();
		$controller->validerAnnonceV($id,$rootUri);
})->name('validerAnnonceV');

$app->put('/annoncesValides/:id', function($id) use($rootUri){ //106 : valider une annonce - bouton VALIDER
		$controller = new \racoin\backend\controller\AnnoncesControllerBE();
		$controller->validerAnnonceV($id,$rootUri);
});

$app->get('/annoncesRefusees/:id', function($id) use($rootUri){ //106 : valider une annonce - bouton REFUSER
		$controller = new \racoin\backend\controller\AnnoncesControllerBE();
		$controller->validerAnnonceR($id,$rootUri);
})->name('validerAnnonceR');

$app->put('/annoncesRefusees/:id', function($id) use($rootUri){ //106 : valider une annonce - bouton REFUSER
		$controller = new \racoin\backend\controller\AnnoncesControllerBE();
		$controller->validerAnnonceR($id,$rootUri);
});

$app->post('/ajoutAdmin', function() use($rootUri){ //route pour ajouter des données ds la table user (dont le mdp hashé)
		$controller = new \racoin\backend\controller\AdminControllerBE();
		$controller->ajoutAdmin($rootUri);
});

//EXECUTION / RENDU
$app->run();