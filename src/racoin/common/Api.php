<?php
namespace racoin\common; 
use Illuminate\Database\Capsule\Manager as DB;

class Api { 

	public static function eloConfigure ( $conf) {
	  $config=parse_ini_file($conf); 
	  if (!$config) throw new Exception ("App::eloConfigure: could not parse config file $conf </br>");

	  $capsule = new DB();
	  $capsule->addConnection($config);

	  $capsule->setAsGlobal();
	  $capsule->bootEloquent();

	 //echo $file ;

	} 


}
