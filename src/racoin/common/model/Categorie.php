<?php
namespace racoin\common\model;


class Categorie extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'categorie';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function annonces () {
		return $this->hasMany('\racoin\common\model\Annonce', 'cat_id') ;
	}



}