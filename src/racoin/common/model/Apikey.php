<?php
namespace racoin\common\model;

class Apikey extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'apikey';
	protected $primaryKey = 'key_api'; /*id*/
	public $timestamps = false;

	public function annonce() {
		return $this->belongsTo('\racoin\common\model\Annonce', 'id') ;
	}
	


}


