<?php
namespace racoin\common\model;

class Annonceur extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'annonceur';
	protected $primaryKey = 'id_annonce';
	public $timestamps = false;

	public function annonces () {
		return $this->hasMany('\racoin\common\model\Annonce', 'id') ;
	}
	




}