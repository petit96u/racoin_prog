<?php
namespace racoin\common\model;

class Annonce extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'annonce';
	protected $primaryKey = 'id';
	public $timestamps = true;

//	protected $hidden = array('passwd','nom_a','prenom_a','mail_a','tel_a'); //ne pas afficher le mot de passe et les informations sur l'annonceur à l'affichage d'une annonce
	
	public function categorie() { 
		return $this->belongsTo('\racoin\common\model\Categorie', 'cat_id') ;
	}
	
	public function departement() {
		return $this->belongsTo('\racoin\common\model\Departement', 'dep_id') ;
	}

	public function apikey() {
		return $this->hasMany('\racoin\common\model\Apikey', 'id') ;
	}	
	
}

