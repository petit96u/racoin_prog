<?php
namespace racoin\common\model;


class Departement extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'departement';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function annonces () {
		return $this->hasMany('\racoin\common\model\Annonce', 'dep_id') ;
	}



}