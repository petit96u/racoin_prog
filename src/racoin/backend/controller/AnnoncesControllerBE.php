<?php

namespace racoin\backend\controller;

use \racoin\common\model\Annonce as Annonce;

class AnnoncesControllerBE { 

/*Statut des annonces :
	statut 1 : en ligne 
	statut 2 : créée
	statut 3 : supprimée*/ 
	
	public function annoncesNonValides($rootUri) { //104 : affichage de la liste des annonces non validées 
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		session_start(); 

		if(isset($_SESSION['admin'])){  
		$annonces = Annonce::select('id', 'created_at', 'nom_a', 'titre', 'prix')->where("status","=",2)
					->orderBy('created_at','ASC')
					->get();
		
		$app->render( 'index.html.twig',[ 'annonce' => $annonces, 'root' => $rootUri ] ); //appel de twig		
		}else{ //sinon pas connecté : erreur 404
				$app->render('404.html.twig',[	'message'=>'Error 404',
											   	'root'=> $rootUri,]);
		}
	}
	
	public function annonceDetaillee($id, $rootUri) { //105 : afficher le détail d'une annonce
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonce = Annonce::select('id', 'titre', 'descriptif', 'ville', 'code_postal', 
							'prix', 'nom_a', 'prenom_a', 'mail_a', 'tel_a', 'created_at')
							->where("id", "=", $id)->first();
							
		//récupère tous les id des annonces non validées dans un tableau pour le bouton "suivante"
		$annonces = Annonce::select('id')->where("status","=",2)
					->orderBy('created_at','ASC')
					->get();
		
		$tab = array(); 			
		foreach($annonces as $annonces){
			$idd = $annonces->id;
			$tab[] = $idd;			
		}
							
		$app->render( 'annonceDetaillee.html.twig',[ 'annonce' => $annonce, 'id' => $id,  'root' => $rootUri, 'annonceV' => ['href'=>$app->urlFor('validerAnnonceV', ['id'=> $id]), 'name'=>'Valider'], 'annonceR' => ['href'=>$app->urlFor('validerAnnonceR', ['id'=> $id]), 'name'=>'Refuser'], 'annonceSuivante' => ['href'=>$app->urlFor('annonceDetaillee', ['id'=> next($tab)]), 'name'=>'Suivante'], 'idAnnonce' =>$id ] );		
	}

	public function validerAnnonceV($id, $rootUri) { //106 : valider une annonce - bouton VALIDER
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonce = Annonce::find($id);	
		$annonce->status = 1; //changement du statut pour le statut "en ligne"
		if($annonce->save()){
			header("Location:".$app->redirect($app->urlFor('index'))); //redirection vers la liste des annonces qu'il reste à valider
		}		
	}	
	
	public function validerAnnonceR($id, $rootUri) { //106 : valider une annonce - bouton REFUSER
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonce = Annonce::find($id);	
		$annonce->status = 3; //changement du statut pour le statut "supprimée"
		if($annonce->save()){
			header("Location:".$app->redirect($app->urlFor('index'))); //redirection vers la liste des annonces qu'il reste à valider
		}		
	}	
}
