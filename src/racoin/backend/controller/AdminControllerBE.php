<?php

namespace racoin\backend\controller;

use \racoin\common\model\Annonce as Annonce;
use \racoin\common\model\User as User;

class AdminControllerBE { 
	
	public function ajoutAdmin($rootUri) { //ajout d'administrateurs dans la base de données pour faire l'authentification
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$data = $app->request->post(); //récupère les données entrées
		$user = new User();
		$user->login = $data['login'];
		
		$pwd = $data['password']; //récupère le mot de passe 
		$hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=> 12)); //hash le mot de passe
		$user->password = $hash;
			
		if ($user->save()){
			$tab = array("L'administrateur ".$data['login']." a bien été enregistré");
			echo json_encode($tab);						
		}
	}
	public function index($rootUri){ 
		$app = \Slim\Slim::getInstance();

		session_start(); 

		if(isset($_SESSION['admin'])){ 
			$app->render('index.html.twig',[
						'root'=> $rootUri,
						'link'=>['href' => $app->urlFor('logout'),
						'name'=>'Déconnexion']]);
		}else{ //sinon si pas connecté 
				$app->render('404.html.twig',['message'=>'Error 404',
											   	'root'=> $rootUri]);
		}
	}
	
	public function login($rootUri){ //formulaire de connexion
		$app = \Slim\Slim::getInstance();

		$data = $app->request->post(); //récupère les données entrées dans le formulaire de la page login.html.twig (login + mdp)
		$message = "";

		session_start();

		if(!isset($_SESSION['admin'])){ //si la session n'existe pas encore
			if($data){ //si des données ont été renvoyées du post
				$admin = User::where("login",$data['nom_adm'])->first(); //trouve ds la bdd l'admin correspondant au pseudo entré ds le formulaire

				if($admin && password_verify($data['pass_adm'],$admin->password)){ //si le pseudo existe bien ds la bdd et que le mdp correspond à ce pseudo
					$_SESSION["admin"] = $admin; 
					header("Location:".$app->redirect('index')); //redirige vers la page index : liste des annonces à valider
				}else{ 
					$message = "Le mot de passe est incorrect. Essayez de nouveau";
				}
			}
		}else{ //si la session existe déjà 
			header("Location:".$app->redirect('index'));
		}

		$app->render('login.html.twig',["message"=>$message,
					'root'=> $rootUri]);
	}
	
	public function logout($rootUri){ //déconnexion
		$app = \Slim\Slim::getInstance();
		session_start(); 

		if(isset($_SESSION['admin'])){ 
			session_destroy(); //détruit la session
			header("Location:".$app->redirect($app->urlFor('login'))); //redirige vers la page de connexion
		}else{ //s'il n'y a pas de session
			$app->render('404.html.twig',['message'=>'Error 404','root'=> $rootUri]);
		}
	}		
}	