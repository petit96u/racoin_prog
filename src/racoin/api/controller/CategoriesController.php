<?php
namespace racoin\api\controller;

use \racoin\common\model\Categorie as Categorie;
use \racoin\common\model\Annonce as Annonce;

class CategoriesController{ 

	public function getCategorie($id) { //1 : fonction pour obtenir 1 ressource de type catégorie
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$cat = Categorie::findOrFail($id); //trouve ou non l'id de la catégorie, si ne trouve pas : créé une erreur
			$arr = $cat->toArray(); //tableau regroupant les données de la catégorie
			//print_r($cat->toArray());
			
			$l = array("href" => "/categories/$id/annonces"); //tableau contenant le lien vers les annonces de la catégorie
			$t = array("annonces" => $l); //tableau regroupant "href" et le lien vers les annonces 
			$arrFinal = array("Catégorie" => $arr, "links" => $t); //tableau final regroupant les données de chaque catégorie et le lien vers ses annonces	
			
			echo json_encode($arrFinal); //affichage du tableau final			
		}
		catch(\Exception $e){ //création de l'exception si l'id de la catégorie n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource catégorie $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
	}
	
	public function getCategories() { //2 : fonction pour obtenir 1 collection de categories	
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');
		
		$categories = Categorie::select('id','libelle')
					->orderBy('libelle','ASC') //ordre alphabétique
					->get();
		$Categorie = array();
		foreach($categories as $categorie){ 
			$uri =  $app->urlFor('categorie', ['id'=> $categorie->id]);
				
			$Categorie[] = [ 'catégorie' => $categorie->toArray(),
					 'links' => [ 'self' => [ 'href' => $uri ]]
					];
		}		
		$arrayFinal = array("Catégories" => $Categorie);			
		echo json_encode($arrayFinal);	
	}		
}