<?php

namespace racoin\api\controller;

use \racoin\common\model\Annonce as Annonce;
use \racoin\common\model\Categorie as Categorie; //pour association annonce-catégorie
use \racoin\common\model\Departement as Departement; 

class AnnoncesController{ 

/*Statut des annonces :
	statut 1 : en ligne 
	statut 2 : créée
	statut 3 : supprimée*/ 
	
	public function getAnnonce($id) { //3 : fonction pour obtenir 1 ressource de type annonce
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$annonce = Annonce::where("status","=",1)->findOrFail($id); //trouve ou non l'id de l'annonce dont le statut doit être en ligne, si ne trouve pas : créer une erreur
			
			//$annonce = Annonce::select('id','titre','descriptif','ville','code_postal','prix')->firstOrFail($id); //trouve ou non l'id de l'annonce, si ne trouve pas : créer une erreur 
			$annoncee = Annonce::select('id','titre','descriptif','ville','code_postal','prix')->where("id","=",$id)->first();
			$arr=array($annoncee);
			$cat_id = Annonce::select("cat_id")->where("id","=",$id)->first();	//récupère l'id de la catégorie (cat_id) correspondant à l'annonce demandée
			$id_cat = $cat_id->cat_id; 
			$cat = Categorie::select('id','libelle')->where('id','=',$id_cat)->first(); //récupère les informations de la catégorie 
			$categorie = array("Catégorie" => $cat);		

			//ajout de la ressource catégorie comme attribut		
			array_push($arr, $categorie);		
					
			$l1 = array("href" => $app->urlFor('categorieAnnonce', ['id'=>$id] ) ); //tableau contenant le lien vers la catégorie de l'annonce						
			$l2 = array("href" => "/annonces/$id/departement"); //tableau contenant le lien vers le département de l'annonce
			$l3 = array("href" => $app->urlFor('coordonneesAnnonceur', ['id'=>$id] )); //tableau contenant le lien vers l'annonceur de l'annonce
			
			$t1 = array("catégorie" => $l1); //tableau regroupant "href" et le lien vers la catégorie
			$t2 = array("département" => $l2); //tableau regroupant "href" et le lien vers le département	
			$t3 = array("annonceur" => $l3); //tableau regroupant "href" et le lien vers l'annonceur	  			
			$arrFinal = array("Annonce" => $arr, "links" => $t1+$t2+$t3); //tableau final regroupant les données de chaque annonce, le lien vers sa catégorie et son département	
			
			echo json_encode($arrFinal); //affichage du tableau final		
		}
		catch(\Exception $e){ //création de l'exception si l'id de l'annonce n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource annonce $id n'existe pas ou n'est pas en ligne"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
	}
	
	public function getAnnonces() { //4 : fonction pour obtenir 1 collection d'annonces
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');
		$paramValueMin = $app->request()->get('min');
		$paramValueMax = $app->request()->get('max');
		if($paramValueMax != '') {
			try{
				echo json_encode($paramValueMax);
				$categories = Annonce::whereBetween('prix', array($paramValueMin,$paramValueMax))
							->orderBy('created_at','DESC')->get();
				
				$arr = ["Toutes les annonces de " => $categories];	
				$arrayFinal = array("Annonces" => $arr);
				echo json_encode($arrayFinal);	
			}
			catch(\Exception $e){
				$app->response->setStatus(404);
				$tab = ["erreur " => "Il n'y a pas d'annonce entre cette fourchette de prix :"];
				echo json_encode($tab);
			}	
		} 
		else{		
			$annonces = Annonce::select('id','titre')->where("status","=",1)
						->orderBy('created_at','DESC')
						->get();
			$Annonce = array();
			foreach($annonces as $annonce){ 
				$uri =  $app->urlFor('annonce', ['id'=> $annonce->id]);
				
				$Annonce[] = [ 'annonce' => $annonce->toArray(),
						 'links' => [ 'self' => [ 'href' => $uri ]]
						];
			}		
			$arrayFinal = array("Annonces" => $Annonce, 'links'=>[]);
			
			echo json_encode($arrayFinal);
		}
	}	
	
	public function getCategorieAnnonce($id) { //6 : obtenir la catégorie d'une annonce
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$annonce = Annonce::findOrFail($id); //trouve ou non l'id de l'annonce, si ne trouve pas : créer une erreur
			
			$cat_id = Annonce::select("cat_id")->where("id","=",$id)->first();	//récupère l'id de la catégorie (cat_id) correspondant à l'annonce demandée
			$id_cat = $cat_id->cat_id; 
			$cat = Categorie::select('id','libelle','descriptif')->where('id','=',$id_cat)->first(); //récupère les informations de la catégorie  
			$arr = array("Catégorie"=>$cat);
			echo json_encode($arr);			
		}
		catch(\Exception $e){ //création de l'exception si l'id de l'annonce n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource annonce $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
	}	
	
	public function postAnnonce() { //11 : ajouter une annonce
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		
		$data = $app->request->post(); //récupère les données de l'annonce entrées par l'utilisateur
		try{
			$annonce = Categorie::findOrFail($data['categorie']); //trouve ou non l'id de la catégorie, si ne trouve pas : créer une erreur
			$tab = array("Titre de l'annonce" => $data['titre'], "Description" => $data['description'], 
			"Ville" => $data['ville'], "Code postal" => $data['codePostal'], "Département" => $data['departement'], "Prix" => $data['prix'], 
			"Catégorie" => $data['categorie'], "Nom annonceur" => $data['nomAnnonceur'], 
			"Prénom annonceur" => $data['prenomAnnonceur'], "Mail annonceur" => $data['mailAnnonceur'],
			"Téléphone annonceur" => $data['telAnnonceur']);
			echo json_encode(["Annonce"=>$tab]); //tableau récupérant les données du post		
		
			$pwd = $data['password']; //récupère le mot de passe entré par l'utilisateur
			$hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=> 12)); //hash le mot de passe reçu
		
			$dpt = Departement::select("numero")->where("numero","=",$data['departement'])->first();

			//ajouter l'annonce dans la base de données
			$annonce = new Annonce();
			$annonce->titre = $data['titre'];
			$annonce->descriptif = $data['description']; 
			$annonce->ville = $data['ville'];
			$annonce->code_postal = $data['codePostal'];
			$annonce->prix = $data['prix'];
			$annonce->nom_a	= $data['nomAnnonceur'];
			$annonce->prenom_a = $data['prenomAnnonceur'];	
			$annonce->mail_a = $data['mailAnnonceur'];	
			$annonce->passwd = $hash;
			$annonce->cat_id = $data['categorie'];
			$annonce->dep_id = $dpt->numero; 
			$annonce->tel_a = $data['telAnnonceur'];
			$annonce->status = 2; //statut "créée" de l'annonce
			$annonce->save(); //enregistre l'annonce dans la base de données
			$annonce_id = $annonce->id;	//récupère l'id de l'annonce qui vient d'être enregistrée				
		}
		catch(\Exception $e){ //création de l'exception si la catégorie n'existe pas
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource catégorie n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
					
		//si l'annonce a été sauvegardée dans la base de données 
		$idDB = Annonce::where('id','=',$annonce_id)->get();
		if ($idDB){
			$app->response->headers->set('Location', $app->urlFor('annonce', ['id'=>$annonce_id] )); //renvoie l'url de la nouvelle annonce
			$app->response->setStatus(201);	// statut 201			
		}		
	}	
	
	public function postAnnonceCategorie($id) { //12 : ajouter une annonce dans une catégorie
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		
		try{
			$categorie = Categorie::findOrFail($id); //trouve ou non l'id de la catégorie, si ne trouve pas : créer une erreur
			
			$data = $app->request->post();
			$tab = array("Titre de l'annonce" => $data['titre'], "Description" => $data['description'], 
			"Ville" => $data['ville'], "Code postal" => $data['codePostal'], "Département" => $data['departement'], "Prix" => $data['prix'], 
			"Catégorie" => $id, "Nom annonceur" => $data['nomAnnonceur'], 
			"Prénom annonceur" => $data['prenomAnnonceur'], "Mail annonceur" => $data['mailAnnonceur'],
			"Téléphone annonceur" => $data['telAnnonceur']);
			echo json_encode(["Annonce"=>$tab]); //tableau récupérant les données du post		
			
			$pwd = $data['password']; //récupère le mot de passe entré par l'utilisateur
			$hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=> 12)); //hash le mot de passe reçu
			
			$dpt = Departement::select("numero")->where("numero","=",$data['departement'])->first();

			//ajouter l'annonce dans la base de données
			$annonce = new Annonce();
			$annonce->titre = $data['titre'];
			$annonce->descriptif = $data['description']; 
			$annonce->ville = $data['ville'];
			$annonce->code_postal = $data['codePostal'];
			$annonce->prix = $data['prix'];
			$annonce->nom_a	= $data['nomAnnonceur'];
			$annonce->prenom_a = $data['prenomAnnonceur'];	
			$annonce->mail_a = $data['mailAnnonceur'];	
			$annonce->passwd = $hash;
			$annonce->cat_id = $id;
			$annonce->dep_id = $dpt->numero; 
			$annonce->tel_a = $data['telAnnonceur'];		
			$annonce->save(); //enregistre l'annonce dans la base de données
			$annonce_id = $annonce->id;	//récupère l'id de l'annonce qui vient d'être enregistrée	
			
		}
		catch(\Exception $e){ //création de l'exception si la catégorie n'existe pas
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource catégorie n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
					
		//si l'annonce a été sauvegardée dans la base de données 
		$idDB = Annonce::where('id','=',$annonce_id)->get();
		if ($idDB){
			$app->response->headers->set('Location', $app->urlFor('annonce', ['id'=>$annonce_id] )); //renvoie l'url de la nouvelle annonce
			$app->response->setStatus(201);	// statut 201			
		}
	}	
			
	public function getAnnoncesCategorie($id){ //5 : collection d'annonces pour une catégorie
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$categorie = Categorie::findOrFail($id); //trouve ou non l'id de la catégorie, si ne trouve pas : créer une erreur
			$annonces = Annonce::select('id','titre')->where("cat_id","=",$id)->where("status","=",1)->orderBy('created_at','DESC')->get();

			$Annonce = array();
			foreach($annonces as $annonce){ 
				$uri = $app->urlFor('annonce', ['id'=> $annonce->id]);
				
				$Annonce[] = [ 'annonce' => $annonce->toArray(),
						'links' => [ 'self' => [ 'href' => $uri ] ]
						];
			}		
			$arrayFinal = array("Annonces" => $Annonce);
			
			echo json_encode($arrayFinal);			
		}
		catch(\Exception $e){ //création de l'exception si l'id de l'annonce n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource catégorie $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}				
	}

	public function deleteAnnonce($id){	//22 : supprimer une annonce	 
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		
		//récupère le mot de passe envoyé dans la requête
		$data = $app->request->delete(); 
		$pwd = $data['password'];
		
		$hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=> 12)); //on hash le mot de passe reçu
		
		try{
			//on récupère l'annonce avec l'id entré dans l'url et le mot de passe 
			$annonce = Annonce::findOrFail($id);
			$password = $annonce->passwd;
		
			if(password_verify($pwd,$password)){ //on vérifie que les 2 mots de passe hashés sont les mêmes				
				$annonce->status = 3; //changement du statut pour le statut "supprimée"
				$annonce->save();
				echo json_encode(["message" => "L'annonce a bien été supprimée"]);
			}
			else{
				$tab = ["erreur " => "Le mot de passe n'est pas correct"]; //tableau json contenant le message d'erreur
				echo json_encode($tab); //affichage du tableau
			}
		}
		catch(exception $e){
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource annonce $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}	
	}		
}
