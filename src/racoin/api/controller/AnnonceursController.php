<?php
namespace racoin\api\controller;

use \racoin\common\model\Annonce as Annonce;
use \racoin\common\model\Categorie as Categorie; 
use \racoin\common\model\Departement as Departement; 

class AnnonceursController{ 

	public function getCoordAnnonceur($id) { //19 : fonction pour obtenir les coordonnées de l'annonceur
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$annonce = Annonce::findOrFail($id); //trouve ou non l'id de l'annonce, si ne trouve pas : créer une erreur 
		
			//récupère données sur l'annonceur correspondant à l'annonce
			$annonceur = Annonce::find($id);
			$tabAnnonceur = array("Nom" => $annonceur->nom_a, "Prénom" => $annonceur->prenom_a, 
			"Mail" => $annonceur->mail_a, "Téléphone" => $annonceur->tel_a); 
			$arr = array("Annonceur" => $tabAnnonceur);
			echo json_encode($arr); 
		}
		catch(\Exception $e){ //création de l'exception si l'id de l'annonce n'est pas trouvée
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource annonce $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}		
		
	}
	
	public function putCoordAnnonceur($id) { //20 : ajouter les coordonnées de l'annonceur
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		
		$data = $app->request->put(); //récupère les données concernant l'annonceur 
		try{
			$annonce = Annonce::findOrFail($id); //trouve ou non l'id de l'annonce
			$tab = array("Nom" => $data['nomAnnonceur'], "Prénom" => $data['prenomAnnonceur'], 
			"Mail" => $data['mailAnnonceur'], "Téléphone" => $data['telAnnonceur']);
			echo json_encode(["Annonceur"=>$tab]); //tableau récupérant les données du put			
			
			//ajouter les données sur l'annonceur dans la base de données
			$annonce = Annonce::find($id); //trouve l'annonce 
			$annonce->nom_a = $data['nomAnnonceur'];
			$annonce->prenom_a = $data['prenomAnnonceur'];	
			$annonce->mail_a = $data['mailAnnonceur'];	
			$annonce->tel_a = $data['telAnnonceur'];				
			$annonce->save(); //enregistre les données dans la base	
		}
		catch(\Exception $e){ //création de l'exception si l'annonce n'existe pas
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource annonce $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}	
	}	
}	
